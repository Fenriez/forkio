const gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify-es').default,
    sourceMaps = require('gulp-sourcemaps'),
    sequence = require('gulp-sequence'),
    clean = require('gulp-clean'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');

gulp.task('indexCopy', () => {
    gulp.src('./index.html')
        .pipe(gulp.dest('./build'));
});

gulp.task('fontsCopy', () => {
    gulp.src('./dev/fonts/*')
        .pipe(gulp.dest('./build/fonts/'));
});

gulp.task('imgmin', () => {
    gulp.src('./dev/images/*')
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{ removeViewBox: true }],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('./build/images/'));
});

gulp.task('clean', () => {
    gulp.src('build', { read: false })
        .pipe(clean());
}); 

gulp.task('sass', () => {
    gulp.src('./dev/scss/*.scss')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourceMaps.init())
        .pipe(sass())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(sourceMaps.write())
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest('./build/css'))
});

gulp.task('serv', ['sass', 'uglify', 'indexCopy', 'fontsCopy'], () => {
    browserSync.init({
        server: './'
    })
    gulp.watch('./dev/js/*.js', ['uglify']);
    gulp.watch('./dev/scss/*.scss', ['sass']);
    gulp.watch('./index.html').on('change', browserSync.reload)
});

gulp.task('uglify', () => {
    gulp.src('./dev/js/*.js')
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest('./build/js/'))
        .pipe(uglify())
        .pipe(rename('bundle.min.js'))
        .pipe(gulp.dest('./build/js/'))
});


gulp.task('default', ['serv']);
